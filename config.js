
const env = 1; //dev
//const env = 2; //test
//const env = 3; //prod

//dev
let rollbar = "280cf6a462f04914a344c011475a2f56";
let widthFactor = 0; 


//test
if (env == 2){
    widthFactor = -6;
}
//prod
if (env == 3){
    rollbar = '3081751ac1c545a69714d9245513bdc1';
    widthFactor = -6;
}


module.exports = {
    env: env,
    rollbar: rollbar,
    port: 3001,
    widthFactor: widthFactor
};
