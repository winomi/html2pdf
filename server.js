'use strict';
const config = require('./config');
const Hapi = require('hapi');
var path = require('path');
global.appRoot = path.resolve(__dirname);


const plugins = require('./lib/loadPlugins.js');

var port = process.env.PORT || config.port;
const server = Hapi.server({
    port:  port,
    //host: 'localhost',
    
    routes: {
        //cors:false
        
        cors: {
            origin: ['*'],
            additionalHeaders: ['x-access-token']        
        }
        
    }
});

//server.route.options.cors.origin = 'ignore';    

const init = async () => {
    var ind = 0;
    for(ind; ind < plugins.length; ind++){
        await server.register(plugins[ind]);
    }
    //await server.inject({method: 'OPTIONS', url:'/policy', headers: {
    //    origin: 'http://localhost/~user/ktree/pdfjs1/',
    //    'access-control-request-method': 'POST',
    //    'access-control-request-headers': ''
    //}});

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

//lPlugins();
init();