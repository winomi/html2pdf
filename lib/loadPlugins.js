const path = require('path');
const _ = require('lodash');
const MrHorse = require('mrhorse');
const Routes = require('hapi-router');
const Rollover = require('rollover');

const config = require('../config.js');

const plugins = new Array();

plugins.push({
    plugin: MrHorse,
    options: {
        policyDirectory: path.join(__dirname, 'policies'),
        defaultApplyPoint: 'onPreHandler'
    }
});

plugins.push({
    plugin: Routes,
    options: {
        routes: 'routes/**/*.js'
    }
});

if (config.env >= 2){ //test or prod
    plugins.push({
        plugin: Rollover,
        options: {
            rollbar: config.rollbar,//"280cf6a462f04914a344c011475a2f56",
            reportErrorResponses: true,
            reportServerLogs: true,
            reportRequestLogs: true
        }
    });
}

// include and initialize the rollbar library with your access token
//var Rollbar = require("rollbar");
//var rollbar = new Rollbar("280cf6a462f04914a344c011475a2f56");
// record a generic message and send it to Rollbar
//rollbar.log("Hello world!");

/*
module.exports = server => new Promise((resolve, reject) => {
        server.register(plugins, (err) => {
        if(err) {
            reject(err);
        } else {
            resolve();
        }
    });
});
*/
/*
module.exports = async function(server) {
    var ind = 0;
    //for(ind; ind < plugins.length; ind++){
    //    await server.register(plugins[ind]);
    //}
    await server.register(goodp);
    await server.register(goodp);
};
*/
module.exports = plugins;