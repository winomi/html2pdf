const Boom = require('boom');

module.exports = async function(pool, query){

    try {
        const [rows, fields] = await pool.query(query);
        return rows
    } catch (err) {
        throw Boom.serverUnavailable('Internal DB Error ' + err.message);
    }
}