const fs = require('fs-extra');
//const path = require('path');
const puppeteer = require('puppeteer');
//const uuidv4 = require('uuid/v4');
const Boom = require('boom');
const Handlebars = require('handlebars');

const widthFactor = require(global.appRoot + '/config.js').widthFactor;

module.exports = async function(data){  

    let fResponse = {
        statusCode: 200
    }

    let tempDirPath = global.appRoot + '/temp/';    
    let tempPdfPath = tempDirPath + '/temp.png';
    let tempHtmlPath = tempDirPath + '/temp';

    try {
        let meta = await fs.readJSON(tempDirPath + data.fundid + '/meta.json');
        let measureType = meta.measure_type;
        let source = await fs.readFileSync(tempDirPath + data.fundid + '/template1.html');
        //let source = await fs.readFileSync(tempDirPath + data.fundid + '/test2.html');
        var template = Handlebars.compile(source.toString());

        const browser = await puppeteer.launch({
            //args: ['--no-sandbox', '--disable-setuid-sandbox'],
            args: [
                '--no-sandbox', 
                '--disable-setuid-sandbox', 
            //    '--font-render-hinting=medium', 
                '--enable-font-antialiasing'
            ],
            timeout:60000,
            headless: true
        });

        let images = [];
        //let pages = [];
        for (let pindex = 0; pindex < meta.number_of_pages; pindex++){
            let pageid = pindex + 1;
            //pages.push(await fs.readFileSync(tempDirPath + data.fundid + '/'+ pageid +'.png'));
            let pageImage = await fs.readFileSync(tempDirPath + data.fundid + '/'+ pageid +'.png');
            var templateData = {
                page: pageImage.toString('base64')
            };
            
            for (var key in meta['page' + pageid]){
                if (meta['page' + pageid].hasOwnProperty(key)){
                    templateData[key] = data[key] || "";
                    //let left = (parseInt(meta['page' + pageid][key].l) + 3).toString();//widthFactor).toString();
                    templateData[key + "_t"] = meta['page' + pageid][key].t + measureType || "";
                    templateData[key + "_l"] = parseInt(meta['page' + pageid][key].l) + measureType || "";
                    templateData[key + "_w"] = parseInt(meta['page' + pageid][key].w) + measureType || "";
                    templateData[key + "_h"] = parseInt(meta['page' + pageid][key].h) + measureType || "";
                }
            }
            
            
            var html = template(templateData);

            //fs.writeFileSync(tempHtmlPath + pageid + '.html', html);

            let page = await browser.newPage();
            await page.setContent(html);
    
            let pdfImage = await page.screenshot({
                omitBackground: false,
                fullPage: true
            });
            
            images.push(pdfImage);

        }

        /*
        var first_name = "מיכאל";
        var last_name = "וינוגרד";
        var id = "29093861";

        var data = { 
            first_name: first_name
            , last_name: last_name
            , id: id
            , page_1: p1.toString('base64')
            //, page_2: p2.toString('base64')
            //, page_3: p3.toString('base64')
            //, page_4: p4.toString('base64')
            //, page_5: p5.toString('base64')
            //, page_6: p6.toString('base64')
            
        };
        var html = template(data);
        //fs.writeFileSync(tempDirPath + 'test2_.html', html);

        //let html = createTable();
        const browser = await puppeteer.launch({
            //args: ['--no-sandbox', '--disable-setuid-sandbox'],
            args: ['--no-sandbox'],
            timeout:60000
        });
        var images = [];

        for (let ii = 0; ii < 6; ii++){
        let page = await browser.newPage();
        //await page.setViewport({width: 794, height: 1110, deviceScaleFactor: 2});
        //await page.goto('file:///' + tempDirPath + 'test2.html');
        await page.setContent(html);
        
        let pdf = await page.screenshot({
            //path: 'screenshot.png',
            omitBackground: false,
            fullPage: true
        });

        images.push(pdf);
        }
        data = { 
            page_1: p2.toString('base64')
            //, page_3: p3.toString('base64')
            //, page_4: p4.toString('base64')
            //, page_5: p5.toString('base64')
            //, page_6: p6.toString('base64')
            
        };
        html = template(data);


        page = await browser.newPage();
        //await page.goto('file:///' + tempDirPath + 'test2.html');
        await page.setContent(html);
        pdf = await page.screenshot({
            //path: 'screenshot.png',
            omitBackground: false,
            fullPage: true
        });
        images.push(pdf);

        */
        await browser.close();

        //fs.writeFileSync(tempPdfPath, images[2]);

        let fResponse = {
            statusCode: 200,
            images: images
        }
    
        return fResponse;
        //return images[0];
    }
    catch (err) {
        console.error(err);
        throw Boom.badImplementation('problem with html/pdf engine ' + err.message);

    }
}

var deleteFolderRecursive = async function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

function createPdf(){
    let html = createTable();
    pdf.create(html).toBuffer(function(err, buffer){
        console.log('This is a buffer:', Buffer.isBuffer(buffer));
    });
}


function createTable(){
    let content = '<html>';
    //content += '<body style="width:200mm"><br />';
    content += '<body ><br />';
        
        content += "<div align='right'>";
        content += "<h4>";
        content += "כותרת";
        content += "</h4>";
        content += "<p>";
        content += "פסקה";
        content += "</p>"
        content += "</div>";

        //mislaka content table
        content += '<div style="page-break-after:always;"></div>';
        content += "<div align='right'>";
        content += "<h4>";
        content += "עמוד שני";
        content += "</h4>";
        content += "<br />";
        content += "<table width='100%' class='page_header'>";
        //header record
        content += "<thead>";
        content += "<tr class='trh'>";
        content += "<th class='tdh'>";
        content += "סכום צפוי";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "יתרת חיסכון מצטבר";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "רמת הסיכון של מסלול ההשקעה";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "דמי ניהול מסך החיסכון";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "דמי ניהול מהפקדה";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "תשואה שנתית ממוצעת";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "הבטחת תשואה";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "שם מסלול ההשקעה ומספרו";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "שם המוצר הפנסיוני ומספרו";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "שם הגוף המוסדי";
        content += "</th>";
        content += "<th  class='tdh'>";
        content += "סוג מוצר";
        content += "</th>";
        content += "<th class='tdh'>";
        content += "המלצה";
        content += "</th>";
        content += "</tr>";
        content += "</thead>";
        content += createRow();
        content += createNimukRow();
        content += createRow();
        content += createNimukRow();
        content += createRow();
        content += createNimukRow();
        content += createRow();
        content += createNimukRow();
        content += createRow();
        content += createNimukRow();
        content += createRow();
        content += createNimukRow();
    content += "</table>";
    content += "</div>";

    content += '<style type="text/css">';
    content += 'table.page_header {width:100%;word-wrap: break-word; text-align: right;border:1px solid black;border-collapse:collapse;}';
    content += 'tr.trh {background-color:#c8cc92;border:1px solid black;border-collapse:collapse;}';            
    content += 'tr.tr1 {border:1px solid black;border-collapse:collapse;}';      
    //content += 'th.tdh {width:8.3%;border:1px solid black;border-collapse:collapse;}';            
    //content += 'td.td1 {width:16.6%;background-color:#c8cc92;border:1px solid black;border-collapse:collapse;}';            
    content += 'th.tdh {border:1px solid black;border-collapse:collapse;}';            
    content += 'td.td1 {background-color:#c8cc92;border:1px solid black;border-collapse:collapse;}';            
    content += 'td.td2 {border:1px solid black;border-collapse:collapse;}';            
    content += '</style>';

    content += '</body>';
    content += '</html>';
    return content;
}

function createRow(){
        //header record
        let content = "<tr class='tr1'>";
        content += "<td class='td2'>";
        content += "סכום צפוי";
        content += "</td>";
        content += "<td class='td2'>";
        content += "יתרת חיסכון מצטבר";
        content += "</td>";
        content += "<td class='td2'>";
        content += "רמת הסיכון של מסלול ההשקעה";
        content += "</td>";
        content += "<td class='td2'>";
        content += "דמי ניהול מסך החיסכון";
        content += "</td>";
        content += "<td class='td2'>";
        content += "דמי ניהול מהפקדה";
        content += "</td>";
        content += "<td class='td2'>";
        content += "תשואה שנתית ממוצעת";
        content += "</td>";
        content += "<td class='td2'>";
        content += "הבטחת תשואה";
        content += "</td>";
        content += "<td class='td2'>";
        content += "שם מסלול ההשקעה ומספרו";
        content += "</td>";
        content += "<td class='td2'>";
        content += "שם המוצר הפנסיוני ומספרו";
        content += "</td>";
        content += "<td class='td2'>";
        content += "שם הגוף המוסדי";
        content += "</td>";
        content += "<td  class='td2'>";
        content += "סוג מוצר";
        content += "</td>";
        content += "<td class='td2'>";
        content += "המלצה";
        content += "</td>";
        content += "</tr>";
    return content;
}

function createNimukRow(){
            //nimuk record
            let content = "<tr>";
            content += "<td class='td1' colspan='2'>";
            content += "השיקולים העיקריים במתן ההמלצה";
            content += "</td>";
            content += "<td class='td2' colspan='10'>";
            content += 'אני , הח"מ, מייפה את כוחו של סוכן הביטוח או היועץ הפנסיוני ומי מטעמו 1, לפנות בשמי לגוף המוסדי 2' +
'המפורט לעיל לשם קבלת מידע 3 אודות מוצרים פנסיוניים 4 ותכנית ביטוח 5 הנמצאות ברשותי מעת לעת' +
'במסגרת ייעוץ פנסיוני או שיווק פנסיוני מתמשך, העברת בקשותיי להצטרפות למוצר או העברת בקשותיי' +
'לביצוע פעולות בו, והכל בהתאם לדין. העברת מידע אודותיי, כאמור לעיל, יכול שתיעשה באמצעות מערכת' +
'סליקה פנסיונית .' +
'אם מנוהלת עבורי תוכנית ביטוח קבוצתית שבעל הפוליסה בה הוא מעביד או ספק שירות, יועבר אודותיה' +
'מידע בלבד ומיופה הכוח לא יורשה לבצע פעולות במוצר';
            content += "</td>";
            content += "</tr>";
            return content;

}