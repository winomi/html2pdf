
module.exports = function(audit, pnum){  
    //let top = pnum * 786 + 30;
    let top = pnum * 786 + 40;
    //let html = '<div style="height:720pt;page-break-after:always;position:absolute;top:' + top + 'pt">';
    let html = '<div style="height:520pt;position:absolute;top:' + top + 'pt;left:20pt;">';

    html += '<div class="head">';
    html += audit.name;
    html += '</div>';
    html += '<div class="normal">Contacted by: ';
    html += audit.audit_contacted_by;
    html += '</div>';
    html += '<div class="normal">Created: ';
    html += audit.audit_documentCreated_at;
    html += '</div>';
    html += '<div class="normal">By: ';
    html += audit.audit_created_by;
    html += '</div>';
    html += '<div class="normal">Status: SIGNED</div>';
    html += '<div class="normal">File id: ';
    html += audit.processid;
    html += '</div>';

    html += '<div class="head">Audit Trail</div>';

    html += '<div class="normal">* Document created by: ';
    html += audit.audit_created_by;
    html += '</div>';
    html += '<div class="subnormal">';
    html += audit.audit_documentCreated_at;
    html += ' UTC, IP address: ';
    html += audit.audit_documentCreated_remoteAddress;
    html += '</div>';

    html += '<div class="normal">* Document viewed by: ';
    html += audit.name;
    html += '</div>';
    html += '<div class="subnormal">';
    html += audit.audit_documentViewed_at;
    html += ' UTC, IP address: ';
    html += audit.audit_documentViewed_remoteAddress;
    html += '</div>';

    html += '<div class="normal">* Document e-signed ';
    html += audit.audit_sign_data.length.toString();
    html += ' time(s) by: ';
    html += audit.name;
    html += '</div>';
    for (let si = 0; si < audit.audit_sign_data.length; si++){
        html += '<div class="subnormal">';
        html += audit.audit_sign_data[si].time;
        html += ' UTC, IP address: ';
        html += audit.audit_sign_data[si].addr;
        html += '</div>';
    }

    html += '<div class="normal">* Document locked by: ';
    html += audit.name;
    html += '</div>';
    html += '<div class="subnormal">';
    html += audit.audit_documentLocked_at;
    html += ' UTC, IP address: ';
    html += audit.audit_documentLocked_remoteAddress;
    html += '</div>';

    html += '</div>';
    return html;
}