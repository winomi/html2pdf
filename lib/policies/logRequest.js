const _ = require('lodash');

module.exports = async function (request, h){
    request.log(['debug', 'input'], _.pick(request, ['id', 'params', 'headers', 'info']));
    return h.continue; // success
}
